# Filter Circuit Boards

This repository contains the schematics, PCB layouts, and derivative files (including Gerber files) of various RF filter boards of the Mark V Holographic Video Monitor.

# License

All files in this repository, including those of subdirectories, are Copyright 2016-2019 BYU ElectroHolography Research Group and are licensed under GNU General Public License v3 ([https://www.gnu.org/licenses/gpl-3.0.en.html](https://www.gnu.org/licenses/gpl-3.0.en.html))

![GPL v3 Logo](https://www.gnu.org/graphics/gplv3-127x51.png "GPL v3")
